cmake_minimum_required(VERSION 3.4 FATAL_ERROR)

include_directories ("${PROJECT_SOURCE_DIR}/JetSelectionHelper")

add_subdirectory(JetSelectionHelper) 
add_subdirectory(AnalysisPayload) 